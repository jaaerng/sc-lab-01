package model;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class RefillMachineFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final int FRAME_WIDTH = 350;
	private static final int FRAME_HEIGHT = 125;
	private static final double DEFAULT_AMOUNT = 0;
	private static final double INITIAL_BALANCE = 0;   

	private JLabel refillLabel, buyLabel, resultLabel;
	private JTextField refillField, buyField;
	private JButton RefillButton, BuyButton;
	private JPanel panel;
	private RefillCard card;

	public RefillMachineFrame() {  
		card = new RefillCard(INITIAL_BALANCE);

		// Use instance variables for components 
		resultLabel = new JLabel("balance: " + card.getBalance());

		// Use helper methods 
		createRefillTextField();
		createRefillButton();
		createBuyTextField();
		createBuyButton();
		createPanel();

		setTitle("UnAerng");
		setSize(FRAME_WIDTH, FRAME_HEIGHT);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setResizable(false);
		setVisible(true);
	}

	private void createRefillTextField() {
		final int FIELD_WIDTH = 10;
		refillLabel = new JLabel("Refill Amount: ");
		refillField = new JTextField(FIELD_WIDTH);
		refillField.setText("" + DEFAULT_AMOUNT);
	}
	
	private void createBuyTextField() {
		final int FIELD_WIDTH = 10;
		buyLabel = new JLabel("Buy Amount: ");
		buyField = new JTextField(FIELD_WIDTH);
		buyField.setText("" + DEFAULT_AMOUNT);
	}

	private void createRefillButton() {
		RefillButton = new JButton("Add Money");
		class AddRefillListener implements ActionListener
		{
			public void actionPerformed(ActionEvent event)
			{
				double amount = Double.parseDouble(refillField.getText());
				card.refill(amount);
				resultLabel.setText("balance: " + card.getBalance());
			}            
		}
		ActionListener listener = new AddRefillListener();
		RefillButton.addActionListener(listener);
	}
	
	private void createBuyButton() {
		BuyButton = new JButton("Buy Product");
		class AddBuyListener implements ActionListener
		{
			public void actionPerformed(ActionEvent event)
			{
				double amount = Double.parseDouble(buyField.getText());
				card.buy(amount);
				resultLabel.setText("balance: " + card.getBalance());
			}            
		}
		ActionListener listener = new AddBuyListener();
		BuyButton.addActionListener(listener);
	}

	private void createPanel() {
		panel = new JPanel();
		panel.setLayout(new GridLayout(3, 3));
		panel.add(refillLabel);
		panel.add(refillField);
		panel.add(RefillButton);
		panel.add(buyLabel);
		panel.add(buyField);
		panel.add(BuyButton);
		panel.add(resultLabel);      
		add(panel);
	} 

}